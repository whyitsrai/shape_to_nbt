*Note: This project is still in it's infancy. As a result of that, breaking changes will be
frequent and there are many missing features. This README is a work-in-progress*

# Shape To NBT

This program aims to simplify the building of desired shapes in Minecraft. Though other tools
with similar functionality do exist, they all give you an image representation that requires
counting. This program gives you a schematic, which you can use with mods such as
[litematica](https://github.com/maruohon/litematica) to render an overlay of the shape in your
Minecraft world, among other things.

You can currently access the website here:
[whyitsrai.gitlab.io/shape_to_nbt](https://whyitsrai.gitlab.io/shape_to_nbt/)

## Limitations

As of now this program only supports generating circles using the bresenham circle algorithm. The
circles are all centered around a block, which results in an odd diameter (regardless of the
radius). More options will be added in the future.

Another limitation is the export file-format. This program currently only supports [Sponge
V2](https://github.com/SpongePowered/Schematic-Specification/blob/master/versions/schematic-2.md)
schematics. This will probably remain the case. If you need it in a different file format, the
mod [litematica](https://github.com/maruohon/litematica) supports schematic conversions.

## For Developers

### Running the Site Locally

In order to run this locally, you need an installation of `rust`, `cargo`, and `wasm-pack`.
Wasm-pack can be installed with the following command: `cargo install wasm-pack`.

Once everything is installed run:

```bash
rustup target add wasm32-unknown-unknown
wasm-pack build --target web
```

You can serve the site using your desired web server (an example is `python3 -m http.server`).

### Using the Library

This is *technically* currently possible, but it is not recommended. There will be some large
breaking API changes in the future. This is also why there is nothing published to
[crates.io](crates.io) as of now.

The library documentation can be found
[here](https://whyitsrai.gitlab.io/shape_to_nbt/doc/shape_to_nbt/).
