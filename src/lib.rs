pub mod bresenham;
pub mod schematics;

use wasm_bindgen::prelude::*;

use crate::bresenham::bresenham_draw_circle;
use crate::schematics::get_schematic;

/// A 3-dimensional point (x, y, z)
#[derive(Debug)]
pub struct Point(i32, i32, i32);

pub struct Circle {
    radius: i32,
    block: String,
    data: Vec<Point>,
}

impl Circle {
    pub fn new(radius: i32, block: &str) -> Circle {
        Circle {
            radius: radius,
            block: block.to_string(),
            data: Vec::<Point>::new(),
        }
    }

    pub fn bresenham_draw(&mut self) {
        self.data = bresenham_draw_circle(&self)
    }
    pub fn get_nbt(self) -> Vec<u8> {
        get_schematic(
            self.radius * 2 + 1,
            self.radius * 2 + 1,
            1,
            &self.block,
            self.data,
        )
    }
}

/// Produces a new circle, draws it using the brensenham algorithm, and then
/// gets the NBT information. This is a binding for wasm to use and not for rust.
/// **NOTE THAT THIS BINDING IS TEMPORARY**
#[wasm_bindgen] // Need to find a better way to interop with wasm
pub fn get_nbt_circle(r: i32, block: &str) -> Vec<u8> {
    let mut c = Circle::new(r, block);
    c.bresenham_draw();
    c.get_nbt()
}
