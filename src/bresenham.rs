use crate::{Circle, Point};

/// Draws a rasterized circle using the Bresenham circle algorithm centered on the block (0, 0)
/// using a given radius. This algorithm does not use square roots, and only uses integers. 
///
/// Note
/// that this function generates circles with an odd diameter for any given radius. This is because
/// the origin of a the circle is a *block* and not a *point*
pub fn bresenham_draw_circle(c: &Circle) -> Vec<Point> {
    let mut result = Vec::new();

    let mut x = 0;
    let mut y = c.radius;
    let mut err = 1 - c.radius;
    let mut err_e = 3;
    let mut err_se = -(c.radius << 1) + 5; // Left bitshift is like *2
    result.push(Point(x, y, 0));
    while x < y {
        if err < 0 {
            err += err_e;
        } else {
            err += err_se;
            err_se += 2;
            y -= 1;
        }
        err_e += 2;
        err_se += 2;

        x += 1;
        result.push(Point(x, y, 0));
    }

    // Get points for entire circle from 1/8th of a circle
    for i in 0..result.len() {
        result.push(Point(result[i].1, result[i].0, 0));
        result.push(Point(-1 * result[i].0, result[i].1, 0));
        result.push(Point(-1 * result[i].1, result[i].0, 0));
        result.push(Point(result[i].0, -1 * result[i].1, 0));
        result.push(Point(result[i].1, -1 * result[i].0, 0));
        result.push(Point(-1 * result[i].0, -1 * result[i].1, 0));
        result.push(Point(-1 * result[i].1, -1 * result[i].0, 0));
    }

    // Append the radius in order to not have negative coordinates
    for i in 0..result.len() {
        result[i] = Point(result[i].0 + c.radius, result[i].1 + c.radius, 0);
    }

    result
}
