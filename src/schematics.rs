use crate::Point;
use flate2::{write::GzEncoder, Compression};
use mcnbt::{ByteOrder, Tag};
use std::io::prelude::*;

/// Generates and returns a gz-compressed Sponge V2 schematic.
///
/// Due to additional variables that a user might want to control (DataVersion, etc),
/// there will be changes made to this function. In other words,
/// ***This API will change in the near future.***
pub fn get_schematic(length: i32, width: i32, height: i32, block: &str, points: Vec<Point>) -> Vec<u8> {
    let mut blocks = vec![0; (length.pow(2)).try_into().unwrap()];
    for pt in points {
        let index: usize = (pt.1 * length + pt.0).try_into().unwrap();
        blocks[index] = 1;
    }

    let tag = Tag::Compound(
        Some("Schematic".to_string()),
        vec![
            Tag::Int(Some("Version".to_string()), 2),
            Tag::Int(Some("DataVersion".to_string()), 3463),
            Tag::Short(Some("Height".to_string()), height.try_into().unwrap()),
            Tag::Short(Some("Length".to_string()), length.try_into().unwrap()),
            Tag::Short(Some("Width".to_string()), width.try_into().unwrap()),
            Tag::IntArray(Some("Offset".to_string()), vec![0, 0, 0]),
            Tag::Compound(
                Some("Palette".to_string()),
                vec![
                    Tag::Int(Some("minecraft:air".to_string()), 0),
                    Tag::Int(Some(block.to_string()), 1),
                ],
            ),
            Tag::ByteArray(Some("BlockData".to_string()), blocks),
        ],
    );

    let raw_data = tag.to_bytes(ByteOrder::BigEndian).unwrap();
    let mut data = GzEncoder::new(Vec::new(), Compression::default());
    data.write_all(&raw_data).unwrap();

    data.finish().unwrap()
}
